#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the maxSubsetSum function below.
def maxSubsetSum(arr):
    index_list = []
    sum_list = []
    for i in range(len(arr)):
        temp = []
        temp.append(i)
        j = i+ 2
        while j < len(arr):
            temp.append(j)
            j += 2
        index_list.append(temp)
    print(index_list)    
    for i in index_list:
        sum_no = 0
        for j in i:
            sum_no += arr[j]
        sum_list.append(sum_no)
    return max(sum_list)
        

if __name__ == '__main__':

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    res = maxSubsetSum(arr)
    print(res)




Subset      Sum
[-2, 3, 5]   6
[-2, 3]      1
[-2, -4]    -6
[-2, 5]      3
[1, -4]     -3
[1, 5]       6
[3, 5]       8