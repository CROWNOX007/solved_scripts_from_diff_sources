#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'rotLeft' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER d
#

def rotLeft(a, d):
    # Write your code here
    for i in range(d):
        temp = a[0]
        for j in range(len(a)-1):
            a[j] = a[j+1]
        a[len(a)-1] = temp
        print(a)
    return a

if __name__ == '__main__':

    first_multiple_input = input().rstrip().split() # enter the value of array length and no of time you want to do left rotation

    n = int(first_multiple_input[0])

    d = int(first_multiple_input[1])

    a = list(map(int, input().rstrip().split()))

    result = rotLeft(a, d)
    print(result)