#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'encryption' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def encryption(s):
    # Write your code here
    length = len(s) # get the len of the string
    root_length = math.sqrt(length) # Find the sqrt of length
    root_length_end_point = int(str(root_length).split(".")[1])
    if root_length_end_point != 0: # what if we don't get any perfect squre value in    root_length
        min_v = int(root_length) # convert it to int, it'll be our row count
        max_v = min_v + 1 # find the +1 of min_v, it'll be our column count
        c = 0 
        while c !=1:
            if min_v * max_v >= length: #for checking whether min_v * max_v >= length or not
                c +=1
            else:
                min_v += 1
        diff = abs(len(s) - (max_v*min_v)) # Find the diff for adding spaces in string
        if diff != 0:
            for i in range(diff):# here we add the spaces because it'll help us to create final matrix
                s += " "         # if we don't do that then sometime we'll get list index out of range error
    else: # if we get perfect squre root in root_length
        min_v = max_v = int(root_length)
    i = 0
    final = [] # final matix (min_v X max_v) order
    for j in range(min_v):
        temp = []
        for k in range(max_v):
            temp.append(s[i])
            i += 1
        final.append(temp)
    final_text = "" # which will contain encrypted text
    column_index = 0 # for accessing column value
    for i in range(max_v):
        for j in range(len(final)):
            if final[j][i] != " ": # if we see extra added space in final matrix then remove it before adding it to final_text
                final_text += final[j][i]
        final_text += " " # add space for separation
    return final_text

if __name__ == '__main__':
    s = input() # haveaniceday
    result = encryption(s)
    print(s)