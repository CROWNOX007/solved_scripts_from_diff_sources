#!/bin/python3

import math
import os
import random
import re
import sys
        
def closest_numbers(numbers):
    numbers.sort()
    final = []
    pair = []
    diff = []
    for i in range(len(numbers)-1):
        temp = [numbers[i], numbers[i+1]]
        diff.append(abs(numbers[i] - numbers[i+1]))
        pair.append(temp)
    min_diff = min(diff)
    for i in range(len(diff)):
        if diff[i] == min_diff:
            final.append(pair[i])
    for i,j in final:
        print(i,j)


if __name__ == '__main__':
    numbers_count = int(input("Enter an integer no (mean no. of elements) : ").rstrip())
    numbers = []
    for i in range(numbers_count):
        numbers_items = int(input(f'Enter {i+1} element : ').rstrip())
        numbers.append(numbers_items)
    print(f'Your given array {numbers}')
    closest_numbers(numbers)
    #print(res)


#input example:
