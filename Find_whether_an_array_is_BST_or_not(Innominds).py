import os, sys

'''Given the root of a binary tree, determine if it is a valid binary search tree (BST)
a. Example
i. Input: root = [2,1,3]'''

def find(BST, index):
    new_index = index-1
    if new_index == 0:
        left = (new_index * 2) + 1
        right = ( new_index * 2) + 2
        if right >= len(BST):
            if BST[new_index] > BST[left]: 
                return find(BST, index -1)
            else:
                return "Above array doesn't follow BST Property."
        elif BST[new_index] > BST[left] and BST[new_index] < BST[right]:
            return find(BST, index -1)
        else:
            return "Above array doesn't follow BST Property."
    else:
            return "Above array does follow BST Property."
    

if __name__ == "__main__":
    value = int(input("Enter the size of an array : ").strip())
    BST = []
    if value > 1:
        for i in range(value):
            item = int(input(f'Enter {i+1} element : ').strip())
            BST.append(item)
    else:
        print(f"Script can't check whether it's BST or not with {value} element.")
        sys.exit()
    print(f"Array provided by the user is : \n {BST}")
    result = find(BST, int(value/2))
    print(result)