"""Given an array of integers, determine whether the array can be sorted in ascending order using only one of the following operations one time.
1.Swap two elements.
2.Reverse one sub-segment.
Determine whether one, both or neither of the operations will complete the task. Output is as follows.
1.If the array is already sorted, output yes on the first line. You do not need to output anything else.
2.If you can sort this array using one single operation (from the two permitted operations) then output yes on the first line and then:
    If elements can only be swapped, d[l]and d[r], output swap l r in the second line.  and  are the indices of the elements to be swapped, assuming that the array is indexed from  to .
    If elements can only be reversed, for the segment d[l...r], output reverse l r in the second line.  and  are the indices of the first and last elements of the subarray to be reversed, 
    assuming that the array is indexed from  to . Here d[l...r]represents the subarray that begins at index  and ends at index , both inclusive.
If an array can be sorted both ways, by using either swap or reverse, choose swap.
3.If the array cannot be sorted either way, output no on the first line.
Example 
a= [2,3,5,4]
Either swap the  and  at indices 3 and 4, or reverse them to sort the array. As mentioned above, swap is preferred over reverse. Choose swap. 
On the first line, print yes. On the second line, print swap 3 4."""

#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'almostSorted' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def almostSorted(arr):
    # Write your code here
    operation_needed = 0
    for i in range(len(arr)-1):
        new_arr = arr[i+1:len(arr)]
        print(new_arr)
        c = 0
        for j in range(len(new_arr)):
            if arr[i]>new_arr[j]:
                c += 1
        operation_needed += c
        print(operation_needed)
    print(operation_needed)
    if operation_needed > 1:
        print('no')
    else:
        print('yes')


if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    almostSorted(arr)
