#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the evenForest function below.

#def up_to_leaf(index, adj_matrix, s_e_n):
#    new_index = []
#    for k in index:
#        for h in range(len(adj_matrix[k])):
#            if h+1 not in s_e_n and adj_matrix[k][h] == 1:
#                new_index.append(h)
#                s_e_n.append(h+1)
#    if len(new_index) > 0:
#        return up_to_leaf(new_index, adj_matrix, s_e_n)
#    else:
#        return s_e_n

def evenForest(t_nodes, t_edges, t_from, t_to):
    ##################################################################################################
    #### I'm not removing this beacuse at first i thought even forst mean a single path must contain
    #### even no of nodes that's why I used the concept of adjacency matrix here and implement it but
    #### after getting wrong answer in test case 1 then I learn more about forest in a graph
    ##################################################################################################
    #adj_matrix = [] # for adjacency matrix
    #r_e = 0 # removal edge count final answer
    #for i in range(t_nodes):
    #    adj_matrix.append([0] * t_nodes) # create the matrix
    #for i in range(len(t_from)): # insert the value accordingly depend on input
    #    adj_matrix[t_from[i] - 1][t_to[i] - 1] = 1
    #    adj_matrix[t_to[i] - 1][t_from[i] - 1] = 1
    #for i in range(len(adj_matrix[0])): # here we are counting no of nodes present in a single path (mean root to leaf (may include more then one child or leaf))
    #    s_e_n = [1] # we always taking the root node for calculation mean level -0
    #    if adj_matrix[0][i] == 1:
    #        s_e_n.append(i+1) # append the index no which is connected to the root node level -1 (mean child nodes of root node)
    #        index = []
    #        index.append(i)
    #        result = up_to_leaf(index, adj_matrix, s_e_n) # if the child nodes has more child in them (mean child node act as parant node) then we call up_to_leaf function
    #        print(result)
    #        if len(result) %2 != 0:
    #            r_e += 1
    #########################################################################################
    #########################################
    r_e = 0 # removal edge count final answer
    parent_child_connection = {} # it'll contain information about which parent has how many child (a child may act as a parent) mean a key will act as
                                 # parent node and it'll contain a list of child
    each_node_connection = {}
    for i in range(len(t_to)):
        if t_to[i] not in parent_child_connection.keys():
            parent_child_connection[t_to[i]] = [t_from[i]] # list of child
        else: parent_child_connection[t_to[i]].append(t_from[i])
    print(parent_child_connection)
    # now we have to see each node (include parent and child node) is connected to how many other nodes.
    for i in range(t_edges+1, 1, -1):
        count = 0
        if i not in parent_child_connection.keys():
                each_node_connection[i] = 1 # 1 mean connected to parent 
        else: 
            each_node_connection[i] = 1 + len(parent_child_connection[i]) # 1 mean connected to parent and len mean child
            # now we have to see if a child act as a parent then how child it has
            for j in parent_child_connection[i]:
                if each_node_connection[j] != 1: # mean it doesn't have any child (it doesn't act as parent)
                    each_node_connection[i] += (each_node_connection[j] - 1) # we already consider the parent node of this child
    print(each_node_connection)
    for i in range(1,t_nodes+1):
        if i in each_node_connection.keys():
            if each_node_connection[i] % 2 == 0:
                r_e += 1
    return r_e
        
    

if __name__ == '__main__':
    t_nodes, t_edges = map(int, input().rstrip().split())

    t_from = [0] * t_edges
    t_to = [0] * t_edges

    for i in range(t_edges):
        t_from[i], t_to[i] = map(int, input().rstrip().split())

    res = evenForest(t_nodes, t_edges, t_from, t_to)
    print(res)


#input example:
