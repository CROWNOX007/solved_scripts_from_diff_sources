#!/bin/python3

import math
import os
import random
import re
import sys

def Topview(index, binary_tree_array, top_view_nodes): # print nodes from leftmost to rightmost
    print(index)
    left_child = ( index * 2) +1
    right_child = ( index * 2) + 2
    if left_child <= len(binary_tree_array) - 1 :
        top_view_nodes = Topview(left_child, binary_tree_array, top_view_nodes)
        top_view_nodes.append(binary_tree_array[index])
        return top_view_nodes
    else:
        top_view_nodes.append(binary_tree_array[index])
        return top_view_nodes
    # another approch is - we already know the no. of nodes in BT so what we'll do is append nodes from right last (rightmost mean append last node then appen len(BT)/2) to left last the reverse

def PostOrder(index, binary_tree_array, right_view_nodes, view):
    if view == 'right' or view == 'Right':
        right_view_nodes.append(binary_tree_array[index]) # always insert root node or parent 
    left_child = ( index * 2) +1
    right_child = ( index * 2) + 2
    #print(i, left_child, left_view_nodes)
    if left_child <= len(binary_tree_array) - 1 and right_child <= len(binary_tree_array) - 1: # enter only if left anf right child idex is < BT array
        if binary_tree_array[right_child] != '': # enter only if right child isn't null
            right_view_nodes = PostOrder(right_child, binary_tree_array, right_view_nodes, 'right')
        elif binary_tree_array[left_child] != '':# enter only if left child isn't null
            #print(i, right_child, left_view_nodes, 'r')
            right_view_nodes = PostOrder(left_child, binary_tree_array, right_view_nodes, 'right')
        else:
            right_view_nodes = PostOrder(index-1, binary_tree_array, right_view_nodes, 'left') # enter only if both the  child of a root node is null then
            # we go to the left child of parent of parent node and send left as view mean we don't want to show left child of parent of parent node
    #elif index > 0:
    #    print(index, index-1)
    #    right_view_nodes = PostOrder(index-1, binary_tree_array, right_view_nodes, 'left')
            
    return right_view_nodes

def PreOrder(index, binary_tree_array, left_view_nodes, view):
    if view == 'left' or view == 'Left':
        left_view_nodes.append(binary_tree_array[index]) # always insert root node or parent 
    left_child = ( index * 2) +1
    right_child = ( index * 2) + 2
    #print(i, left_child, left_view_nodes)
    if left_child <= len(binary_tree_array) - 1 and right_child <= len(binary_tree_array) - 1: # enter only if left anf right child idex is < BT array
        if binary_tree_array[left_child] != '': # enter only if left child isn't null
            left_view_nodes = PreOrder(left_child, binary_tree_array, left_view_nodes, 'left')
        elif binary_tree_array[right_child] != '':# enter only if right child isn't null
            #print(i, right_child, left_view_nodes, 'r')
            left_view_nodes = PreOrder(right_child, binary_tree_array, left_view_nodes, 'left')
        else:
            left_view_nodes = PreOrder(index+1, binary_tree_array, left_view_nodes, 'right') # enter only if both the  child of a root node is null then we go the
            # we go to the right child of parent of parent node and send right as view mean we don't want to show right child of parent of parent node
    return left_view_nodes

def print_view(binary_tree_array, nodes, view):
    left_view_nodes = [] # will store all the nodes that can be seen for left view of a binary tree
    right_view_nodes = []# will store all the nodes that can be seen for right view of a binary tree
    top_view_nodes = []
    #tree_level = 0 # we already know the formula for like l = (log 5 base 2) if nodes = 5
    #for i in range(nodes):
    #    if pow(2, i) > nodes:
    #        tree_level = i # calculate the level of the BT
    #        break
    if binary_tree_array[0] != '':
        if view == 'left' or view == 'Left':
            left_view_nodes = PreOrder(0, binary_tree_array, left_view_nodes, view) # it'll take parent index, BT array and  view
            return left_view_nodes
        elif view == 'right' or view == 'Right':
            # please provide 2^l - 1 no of nodes 
            right_view_nodes = PostOrder(0, binary_tree_array, right_view_nodes, view) # it'll take parent index, BT array and  view
            return right_view_nodes
        elif view == 'Top' or view == 'top':
            if '' not in binary_tree_array:
                top_view_nodes = Topview(0, binary_tree_array, top_view_nodes)
                return top_view_nodes
            else:
                return "Binary tree has null node in it."
    else:
        return "Binary Tree doesn't have any root node. So, no left or right view possible."
        
    

if __name__ == '__main__':
    t_nodes = int(input("Enter the no. of nodes that will present in BT : ").rstrip())

    binary_tree_array = [0] * t_nodes # create the BT array

    for i in range(t_nodes): # insert nodes in BT array
        binary_tree_array[i] = input(f'Enter {i+1} element of a BT : ').rstrip()
    view_output = input("\nWhich type of view do you want to print? (Mean left or right or top(each node must've two child))\n").rstrip()
    result = print_view(binary_tree_array, t_nodes, view_output)
    print(f'\nOutput :- \n{result}')
    #print(binary_tree_array)


#input example:
#Enter the no. of nodes that will present in BT : 7
#Enter 1 element of a BT : 1    
#Enter 2 element of a BT : 2
#Enter 3 element of a BT : 3
#Enter 4 element of a BT : 
#Enter 5 element of a BT : 4
#Enter 6 element of a BT : 
#Enter 7 element of a BT : 

#Which type of view do you want to print? (Mean left or right)
#right
#or
#left


#Output :- 
#['1', '3', '4'] for right
#['1', '2', '4'] for left