def encoded_string(s):
    if s != '':
        s_list = []
        s_list[:0] = s
        en_s = ''
        count = 0
        for i in range(len(s_list)):
            if i == 0:
                prev_string = s_list[i]
                count += 1
            elif s_list[i] == prev_string:
                count += 1
            else:
                en_s += str(count) + prev_string
                prev_string = s_list[i]
                count = 1
        return en_s
    else:
        return 'Empty string is provided'

if __name__ == '__main__':
    s = input("Insert the normal string : ").strip() #'aaaabbbbbbaaccccccgdfd'
    en_s = encoded_string(s)
    print(f'Encoded string of {s} is {en_s}.')
