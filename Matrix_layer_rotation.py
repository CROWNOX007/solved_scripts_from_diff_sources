#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'matrixRotation' function below.
#
# The function accepts following parameters:
#  1. 2D_INTEGER_ARRAY matrix
#  2. INTEGER r
#

def matrixRotation(matrix, r):
    # Write your code here
    row = len(matrix)
    column = len(matrix[0])
    no_of_layer = min(row, column) // 2 # don't take the float no take the int value mean 3 // 2 = 1
    for i in range(no_of_layer):
        



if __name__ == '__main__':
    first_multiple_input = input().rstrip().split()

    m = int(first_multiple_input[0])

    n = int(first_multiple_input[1])

    r = int(first_multiple_input[2])

    matrix = []

    for i in range(m):
        matrix.append(list(map(int, input(f'Enter the {i+1} rows value with {n} columns : ').rstrip().split())))
    print("Matrix provided by you : ")
    for i in matrix:
        print(i)
    result = matrixRotation(matrix, r)
    print(result)
