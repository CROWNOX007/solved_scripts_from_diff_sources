#!/bin/python3

import math
import os
import random
import re
import sys



# Complete the maxTickets function below.
def maxTickets(tickets):
    tickets.sort()
    subsequense = []
    already = []
    for i in range(len(tickets)-1):
        new = []
        if tickets[i] not in already:
            already.append(tickets[i])
            for j in range(len(tickets)):
                if tickets[i] - tickets[j] == 0 or tickets[i] - tickets[j] == 1:
                    if tickets[i] not in new:
                        new.append(tickets[i])
                        already.append(tickets[j])
                        new.append(tickets[j])
                    else:
                        already.append(tickets[j])
                        new.append(tickets[j])
        subsequense.append(len(new))
    return max(subsequense)
    
            
if __name__ == '__main__':
    tickets_count = int(input().strip())

    tickets = []

    for _ in range(tickets_count):
        tickets_item = int(input().strip())
        tickets.append(tickets_item)

    res = maxTickets(tickets)
    print(res)
