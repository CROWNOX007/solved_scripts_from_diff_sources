def efficientJanitor(weight):
    # Write your code here
    index = []
    trip = []
    for i in range(len(weight)):
        count =[]
        if i not in index:
            amount = weight[i]
            index.append(i)
            if i != len(weight) - 1:
                count.append(i)
                for j in range(i+1,len(weight)):
                    amount += weight[j]
                    if amount > 3.00:
                        break
                    else:
                        index.append(j)
                        count.append(j)
                trip.append(count)
            else:
                trip.append([i])                        
    #print(trip)
    return len(trip)

if __name__ == '__main__':
    weight = [2.01, 2.991, 1.32, 2.4, 1.00, 1.32]
    result = efficientJanitor(weight)
    print(result)