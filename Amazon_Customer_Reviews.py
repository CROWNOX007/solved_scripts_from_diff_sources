import sys
def suggestion_search(repositoryList, customer_item):
    result =[] # final list
    count = 1 # no of word we will compair each time
    customer_word = '' # each time it will add new word
    for i in customer_item:
        customer_word += i # adding new word 
        if count>=2: # so suggestion only if customer provide 2 words
            each_time_response = [] # each time how many word we can suggest to the customer
            for j in repositoryList:
                #print(customer_word, j[0:c])
                if customer_word == j[0:count]: # each time we will compair customer provided word with each element of repositoryList from left to right
                    each_time_response.append(j)
            each_time_response.sort() # sort them each time or we can sort repositoryList (one time)
            #print(eac)
            result.append(each_time_response) # store each_time_response 
        count += 1 # increase the count
        #print(result)
    return result

if __name__ == '__main__':
    repository_count = int(input().strip()) # no of elements in repositoryList
    repositoryList = []
    for _ in range(repository_count):
        repository_item = input().strip() # provide input
        repositoryList.append(repository_item) # append input
    customer_item = input().strip() # input
    result = suggestion_search(repositoryList, customer_item) # call suggestion_search function
    print(result)
    # not complete yet