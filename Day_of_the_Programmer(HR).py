#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'dayOfProgrammer' function below.
#
# The function is expected to return a STRING.
# The function accepts INTEGER year as parameter.
#

def dayOfProgrammer(year):
    # Write your code here
    if year % 4 == 0:
        days = 244
        pro_day = 256 - days
        r_value = str(pro_day) + '.09.' + str(year)
        return r_value
    else:
        days = 243
        pro_day = 256 -days
        r_value = str(pro_day) + '.09.' + str(year)
        return r_value
    

if __name__ == '__main__':
    year = int(input().strip())
    result = dayOfProgrammer(year)
    print(result)
