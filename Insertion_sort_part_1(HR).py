#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'insertionSort1' function below.
#
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER_ARRAY arr
#

def insertionSort1(n, arr):
    # Write your code here
    value = arr[n-1] # value've to be sorted
    #arr[n-1] = arr[n-2]
    #print(arr)
    print("Output : ")
    for i in range(n-2, -1,-1): # it'll run n-2 to 0
        if arr[i] > value :
            arr[i+1] = arr[i]
            if i == 0:
                print(' '.join(map(str, arr)))
                arr[i] = value
                print(' '.join(map(str, arr)))
                break
        elif arr[i] < value :
            arr[i+1] = value
            print(' '.join(map(str, arr)))
            break
        print(' '.join(map(str, arr)))

if __name__ == '__main__':
    n = int(input("Enter the size of an array : ").strip())

    arr = list(map(int, input("Enter the array elements with space separated : ").rstrip().split()))

    insertionSort1(n, arr)

"""
Enter the size of an array : 5
Enter the array elements with space separated : 2 4 6 8 3
Sample Output
2 4 6 8 8 
2 4 6 6 8 
2 4 4 6 8 
2 3 4 6 8 


"""