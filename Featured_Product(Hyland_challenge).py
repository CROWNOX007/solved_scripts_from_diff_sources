import os, sys

def featured(products):
    unique_products = list(set(products))
    featured_products = []
    count_p = []
    for i in unique_products:
        count_p.append(products.count(i))
    max_count = max(count_p)
    for i in unique_products:
        if products.count(i) == max_count:
            featured_products.append(i)
    featured_products.sort()
    return featured_products.pop()
    

if __name__ == "__main__":
    Product_count = int(input("Enter the size of a Product array : ").strip())
    products = []
    for i in range(Product_count):
        item = input(f'Enter {i+1} product : ').strip()
        products.append(item)
    print(f"\nThe Product array provided by the user is : \n {products}")
    result = featured(products)
    print(f"\nThe featured product of the following day is {result}")


'''The input:
The Product array provided by the user is : 
 ['red', 'blue', 'green', 'white', 'yellow', 'pink', 'white']
output :
The featured product of the following day is white'''