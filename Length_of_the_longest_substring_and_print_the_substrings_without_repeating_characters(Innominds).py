import os,sys
'''Given a string str, find the length of the longest substring without repeating characters. 

For “ABDEFGABEF”, the longest substring are “BDEFGA” and “DEFGAB”, with length 6.
For “BBBB” the longest substring is “B”, with length 1.
Input: s = "abcabcbb"'''

def find(String):
    L_Strings = []
    max_length = []
    unique_string = set(list(String))
    for i in range(len(String)-1):
        new_string = ""
        for j in range(i, len(String)):
            if String[j] not in new_string:
                new_string += String[j]
            else:
                break
        L_Strings.append(new_string.strip())
        max_length.append(len(new_string.strip()))
    return L_Strings, max(max_length)
        

if __name__ == "__main__":
    String = input("Enter a String : ").strip()
    print(f"String provided by the user is : \n{String}")
    result, max_length = find(String)
    print(f"Length of the longest substring is : {max_length}")
    print("Those or The Longest Substring('s) Without Repeating Characters is/are : ")
    for i in result:
        if len(i) == max_length:
            print(i)
    #print(result, max_length)