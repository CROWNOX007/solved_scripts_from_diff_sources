import math
import os
import random
import re
import sys


#
# Complete the 'kaprekarNumbers' function below.
#
# The function accepts following parameters:
#  1. INTEGER p
#  2. INTEGER q
#

def kaprekarNumbers(p, q):
    # Write your code here
    final = "" # for final output
    for i in range(p, q+1):
        left_value = "" 
        right_value = ""
        d = len(str(i)) # len of the current no.
        power = str(pow(i,2)) # her we get (no.)^2
        if len(power) != 1: # enter - only if we get a no. in power which have length > 1
            for j in range(len(power)):
                if j < len(power) - d: # enter - only j < len(power) - d
                    left_value += power[j] # left_value must contain the remaning digit
                else:
                    right_value += power[j] # right_value must contain d length digit from power
            add = int(right_value) + int(left_value) # add them we can follow any order
        else:
            add = int(power)
        if add == i:
            final += str(add) + " "    
    if final == "": # if can't find any no. then only enter
        return "INVALID RANGE"
    else:
        return final
        

if __name__ == '__main__':
    p = int(input().strip())

    q = int(input().strip())

    result = kaprekarNumbers(p, q)
    print(result)