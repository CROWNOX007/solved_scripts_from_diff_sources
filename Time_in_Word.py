import sys

global time_in_word, word

time_in_word = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fiveteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
'twenty', 'twenty one', 'twenty two', 'twenty three', 'twenty four', 'twenty five', 'twenty six', 'twenty seven', 'twenty eight', 'twenty nine']


def time_to_word(hour, minute):
    if minute < 1:
        word = time_in_word[hour - 1] + " o' clock"
    elif minute >=1 and minute <= 30:
        if minute == 15:
            word = "quarter past " + time_in_word[hour - 1]
        elif minute == 30:
            word = "half past " + time_in_word[hour - 1]
        else:
            if minute == 1:
                word = time_in_word[minute - 1] + " minute past " + time_in_word[hour - 1]
            else:
                word = time_in_word[minute - 1] + " minutes past " + time_in_word[hour - 1]
    elif minute > 30:
        if minute == 45:
            word = "quarter to " + time_in_word[hour]
        else:
            new_minute = 60 - minute
            word = time_in_word[new_minute - 1] + " minutes to " + time_in_word[hour]
    return word



if __name__ == "__main__":
    hour = input()
    minute = input()
    result = time_to_word(hour, minute)
    print(result)
    