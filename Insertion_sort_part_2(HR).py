#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'insertionSort1' function below.
#
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER_ARRAY arr
#

def insertionSort2(n, arr):
    # Write your code here
    for i in range(n-1):
        if arr[i] > arr[i+1]:
            temp = arr[i]
            arr[i] = arr[i+1]
            arr[i+1] = temp
            if i > 0 and arr[i] < arr[i-1]: # if the current position is less the previous postion the do insertion sort from the current position
                for j in range(i, 0, -1): # mean it'll not consider 0
                    if arr[j] < arr[j-1]:
                        temp = arr[j]
                        arr[j] = arr[j-1]
                        arr[j-1] = temp
                    #if i == 0:
                    #    break
        print(' '.join(map(str, arr)))

if __name__ == '__main__':
    n = int(input("Enter the size of an array : ").strip())

    arr = list(map(int, input("Enter the array elements with space separated : ").rstrip().split()))

    insertionSort2(n, arr)

"""
Enter the size of an array : 6
Enter the array elements with space separated : 1 4 3 5 6 2 
Sample Output
1 4 3 5 6 2 
1 3 4 5 6 2 
1 3 4 5 6 2 
1 3 4 5 6 2 
1 2 3 4 5 6 


"""