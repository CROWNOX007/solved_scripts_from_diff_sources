import math
import os
import random
import re
import sys

def matrix_rotation(matrix, rotate_degree):
    if rotate_degree == '90':
        final_result = [] # store the final result
        for i in range(len(matrix), 0, -1):
            temp = [] # temp list will contain the rotate list
            for j in range(len(matrix)):
                temp.append(matrix[j][i-1])
            final_result.append(temp)
        return final_result
    elif rotate_degree == '180':
        final_result = matrix # store the final result 
        for i in range(len(matrix), 0, -1):
            temp = [] # temp list will contain the rotate list
            for j in range(len(matrix)):
                temp.append(matrix[j][i-1]) 
    elif rotate_degree == '270':
        return matrix
    elif rotate_degree == '360':
        return matrix

if __name__ == '__main__':
    matrix = [[1,2,3],[4,5,6],[7,8,9]]#[[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]#
    print("Your matrix : ")
    for i in matrix:
        print(i)
    rotate_degree = input("Please enter the degree (like 90 or 180 or 270) : ").rstrip()
    result = matrix_rotation(matrix, rotate_degree)
    print("Output : ")
    for i in result:
        print(i)