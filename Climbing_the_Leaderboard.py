#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'climbingLeaderboard' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY ranked
#  2. INTEGER_ARRAY player
#

def climbingLeaderboard(ranked, player):
    # Write your code here
    r = [] #for storing rank
    result = []
    max_v = max(ranked) # max value
    min_v = min(ranked) # min value
    for i in range(len(ranked)):
        if ranked[i] == max_v:
            r.append(1)
        elif ranked[i] < max_v and ranked[i] > min_v and len(r) == 1:
            r.append(len(r)+1)
        elif ranked[i] < max_v and ranked[i] > min_v and len(r) != 1:
            if ranked[i] == ranked[i-1]:
                r.append(max(r))
            else:
                r.append(max(r)+1)
        elif ranked[i] == min_v:
            r.append(max(r)+1)  
    l_r = zip(r,ranked)  
    for k in player:
        if k >= max_v:
            result.append(min(r))
        elif k == min_v:
            result.append(max(r))
        #elif k < min_v:
            #result.append(max(result)+1)
        elif k < max_v and k > min_v:
            for i,j in l_r:
                if k == j:
                    result.append(i)
                    break
                elif k > j:
                    for l in range(len(ranked)):
                        if k < ranked[l]:
                            result.append(i - 1)
                            break
    return r

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    ranked_count = int(input().strip())

    ranked = list(map(int, input().rstrip().split()))

    player_count = int(input().strip())

    player = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(ranked, player)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
