"""There are a number of people who will be attending ACM-ICPC World Finals. Each of them may be well versed in a number of topics. 
Given a list of topics known by each attendee, presented as binary strings, determine the maximum number of topics a 2-person team can know. 
Each subject has a column in the binary string, and a '1' means the subject is known while '0' means it is not. Also determine the number of teams that know the maximum number of topics.
Return an integer array with two elements. The first is the maximum number of topics known, and the second is the number of teams that know that number of topics.
Example
4 5
10101
11100
11010
00101

o/p- 
5
2
Explanation
Calculating topics known for all permutations of 2 attendees we get:
(1,2) -> 4
(1,3) -> 5
The 2 teams (1, 3) and (3, 4) know all 5 topics which is maximal.
"""
#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'acmTeam' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts STRING_ARRAY topic as parameter.
#

def acmTeam(topic):
    # Write your code here
    #print(topic)
    subject = []
    for i in range(len(topic)-1):
        for k in range(i+1, len(topic)):
            a = topic[i]
            b = topic[k]
            sub = 0
            for j in range(max(len(a), len(b))):
                if a[j] == '1' or b[j] =='1':
                    sub += 1
            subject.append(sub)
    print(subject)
    return [max(subject), subject.count(max(subject))]

if __name__ == '__main__':
    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    topic = []

    for _ in range(n):
        topic_item = input()
        topic.append(topic_item)

    result = acmTeam(topic)
    print(result)
